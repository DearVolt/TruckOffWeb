﻿angular
   .module('LastStopApp')
    .controller('clientDashboardController',
        function ($scope) {

            $scope.awaitingRequests = [
                {
                    destination: 'Cape Town',
                    price: 'R150',
                    items: ['Steel beams', 'Wooden planks'],
                    deliverableByDate: '28-07-2016',
                    date: 'Wed, 25th of July 2016',
                    time: '08:00'
                },
                {
                    destination: 'Durban',
                    price: 'R185',
                    items: ['Couch'],
                    deliverableByDate: '30-07-2016',
                    date: 'Fri, 27th of July 2016',
                    time: '10:00'
                }
            ]

        });
