﻿angular
   .module('LastStopApp')
    .controller('createNewRequestController',
        function ($scope) {

            $scope.selectedItems = [];

            var self = this;
            self.readonly = false;
            self.selectedItem = null;
            self.searchText = null;
            self.querySearch = querySearch;
            self.items = loadItems();
            self.selectedVegetables = [];
            self.numberChips = [];
            self.numberChips2 = [];
            self.numberBuffer = '';
            self.autocompleteDemoRequireMatch = true;
            self.transformChip = transformChip;
            /**
             * Return the proper object when the append is called.
             */
            function transformChip(chip) {
                // If it is an object, it's already a known chip
                if (angular.isObject(chip)) {
                    return chip;
                }
                // Otherwise, create a new one
                return { name: chip, type: 'new' }
            }
            /**
             * Search for vegetables.
             */
            function querySearch(query) {
                var results = query ? self.items.filter(createFilterFor(query)) : [];
                return results;
            }
            /**
             * Create filter function for a query string
             */
            function createFilterFor(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(item) {
                    return (item._lowername.indexOf(lowercaseQuery) === 0) ||
                        (item._lowertype.indexOf(lowercaseQuery) === 0);
                };
            }
            function loadItems() {
                var items = [
                  {
                      'name': 'Wooden Planks',
                      'type': 'Materials'
                  },
                  {
                      'name': 'Steel Beams',
                      'type': 'Materials'
                  },
                  {
                      'name': 'Couch',
                      'type': 'Furniture'
                  },
                  {
                      'name': 'Bed',
                      'type': 'Furniture'
                  },
                  {
                      'name': 'Chair',
                      'type': 'Furniture'
                  }
                ];
                return items.map(function (item) {
                    item._lowername = item.name.toLowerCase();
                    item._lowertype = item.type.toLowerCase();
                    return item;
                });

            }
        });
