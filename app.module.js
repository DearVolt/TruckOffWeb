/**
 * Created by Murray on 29-Jul-16.
 */


var app = angular.module('LastStopApp', [
    'ngRoute',
    'ngMaterial'
]).controller('AppController', AppController);


app.config(function ($routeProvider) {
    $routeProvider
    .when('/login', {
        templateUrl: 'views/login/login.html',
        controller: 'loginController'
    })
    .when('/register/client', {
        templateUrl: 'views/clientRegistration/clientRegistration.tpl.html',
        controller: 'clientRegistrationController'
    })
    .when('/register/driver', {
        templateUrl: 'views/driverRegistration/driverRegistration.tpl.html',
        controller: 'driverRegistrationController'
    })
    .when('/client/dashboard', {
        templateUrl: 'views/clientDashboard/clientDashboard.tpl.html',
        controller: 'clientDashboardController'
    })
    .when('/client/create-delivery-request', {
        templateUrl: 'views/createNewDeliveryRequest/createNewRequest.tpl.html',
        controller: 'createNewRequestController'
    })
    .otherwise({ redirectTo: '/login' });
}
);

function AppController($scope) {
}

