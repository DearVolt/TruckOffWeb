﻿angular
  .module('LastStopApp')

  .directive('lsHeader',  function ($mdSidenav) {
      return {
          scope: {
          },
          restrict: 'E',
          transclude: true,
          replace: true,
          templateUrl: '/components/Header/header.tpl.html',
          link: linkFunction
      };


  })

function linkFunction(scope, element, attrs) {
    scope.toggleMenu = buildToggler('navmenu');

    function buildToggler(navID) {
        return function () {
            // Component lookup should always be available since we are not using `ng-if`
            $mdSidenav(navID)
              .toggle()
        }
    }
}